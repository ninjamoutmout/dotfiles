#!/usr/bin/env bash

file="$HOME/Pictures/Screenshots/screenshot-$(date +%y-%m-%d-%H-%M-%S).png"
notify_prog="notify-send -i spectacle"

usage="$(basename "$0") -- a script to take screenshots in a wayland environment using grim and slurp
where :
    -r                  : region screenshot
    -c                  : show cursor in the screenshot
    -f <file_path>      : use a specific file (default $HOME/Pictures/screenshots/screenshot-$(date +%y-%m-%d-%H-%M).png)
    -h                  : show this help
This script depends on :
- grim
- slurp 
- coreutils
- bash (obviously)"

while getopts "rcfh" option; do
    case "${option}" in
        f) file="${OPTARG}"
           ;;
        r) region=true
           ;;
        c) cursor=true
           ;;
        h) echo "${usage}"
           exit 0
           ;;
        *) echo "Wrong argument, use $(basename "$0") -h for more info"
           $notify_prog wl-screenshot "Wrong argument, use $(basename "$0") -h for more info"
           exit 1
           ;;
    esac
done

if [[ $region ]] && [[ $cursor ]]; then
    grim -g "$(slurp)" -c "$file"
    if [[ $? != 0 ]]; then
        $notify_prog wl-screenshot "Screenshot failed"
        echo "Screenshot failed"
        exit 1
    fi
    $notify_prog wl-screenshot "Screenshot saved to $file" 2>/dev/null
    echo "Screenshot saved to $file"
    exit 0
elif [[ $region ]]; then
    grim -g "$(slurp)" "$file"
    if [[ $? != 0 ]]; then
        $notify_prog wl-screenshot "Screenshot failed"
        echo "Screenshot failed"
        exit 1
    fi
    $notify_prog wl-screenshot "Screenshot saved to $file" 2>/dev/null
    echo "Screenshot saved to $file"
    exit 0
elif [[ $cursor ]]; then
    grim -c "$file"
    if [[ $? != 0 ]]; then
        $notify_prog wl-screenshot "Screenshot failed"
        echo "Screenshot failed"
        exit 1
    fi
    $notify_prog wl-screenshot "Screenshot saved to $file" 2>/dev/null
    echo "Screenshot saved to $file"
    exit 0
else
    grim "$file"
    if [[ $? != 0 ]]; then
        $notify_prog wl-screenshot "Screenshot failed" 2>/dev/null
        echo "Screenshot failed"
        exit 1
    fi
    $notify_prog wl-screenshot "Screenshot saved to $file" 2>/dev/null
    echo "Screenshot saved to $file"
fi

