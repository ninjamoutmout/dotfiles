require("plugins")
require("config")


vim.g.mapleader = ' '
vim.g.maplocalleader = ' '


vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.shiftround = true
vim.opt.expandtab = true


