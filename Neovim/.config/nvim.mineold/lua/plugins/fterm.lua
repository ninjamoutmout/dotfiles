-- FTerm.nvim --
return {
	"numToStr/FTerm.nvim",
	lazy = true,
	opts = {
		dimensions = {
			height = 1, -- Height of the terminal window
			width = 0.5, -- Width of the terminal window
			x = 1, -- X axis of the terminal window
			y = -0.1, -- Y axis of the terminal window
		},
		border = "single",
	},
}


