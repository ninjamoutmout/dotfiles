-- typst.vim --
return {
    {
        "kaarmu/typst.vim",
        ft = "typst",
        config = function() vim.g.typst_pdf_viewer = "zathura" end,
    },
    {
        "chomosuke/typst-preview.nvim",
        ft = "typst",
        version = "0.1.*",
        build = function() require("typst-preview").update() end,
    },
}


