return {
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "main",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons",
      "MunifTanjim/nui.nvim",
    },
    config = function()
      vim.keymap.set("n", "<C-t>", "<cmd>Neotree toggle<cr>", { desc = "Open Neotree" })
      -- Functions for neo-tree
      function open_neo_tree(data)
        -- buffer is a directory
        local directory = vim.fn.isdirectory(data.file) == 1
        if not directory then
          return
        end
        -- create a new, empty buffer
        vim.cmd.enew()
        -- wipe the directory buffer
        vim.cmd.bw(data.buf)
        -- change to the directory
        vim.cmd.cd(data.file)
        -- open the tree
        require("neo-tree").reveal_in_split()
      end

      local function getTelescopeOpts(state, path)
        return {
          cwd = path,
          search_dirs = { path },
          attach_mappings = function(prompt_bufnr, map)
            local actions = require("telescope.actions")
            actions.select_default:replace(function()
              actions.close(prompt_bufnr)
              local action_state = require("telescope.actions.state")
              local selection = action_state.get_selected_entry()
              local filename = selection.filename
              if filename == nil then
                filename = selection[1]
              end
              -- any way to open the file without triggering auto-close event of neo-tree?
              require("neo-tree.sources.filesystem").navigate(state, state.path, filename)
            end)
            return true
          end,
        }
      end
      -- Setup
      require("neo-tree").setup({
        filesystem = {
          window = {
            position = "left",
            width = 30,
            mappings = {
              ["tf"] = "telescope_find",
              ["tg"] = "telescope_grep",
            },
          },
          commands = {
            telescope_find = function(state)
              local node = state.tree:get_node()
              local path = node:get_id()
              require("telescope.builtin").find_files(getTelescopeOpts(state, path))
            end,
            telescope_grep = function(state)
              local node = state.tree:get_node()
              local path = node:get_id()
              require("telescope.builtin").live_grep(getTelescopeOpts(state, path))
            end,
          },
        },
      })
      -- Opens neo-tree instead of netrw (wont work if neotree is lazyloaded, I only place that here as a snippet)
      -- vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_neo_tree })
    end,
  },
  {
    -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = {
      -- Snippet Engine & its associated nvim-cmp source
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',

      -- Adds LSP completion capabilities
      'hrsh7th/cmp-nvim-lsp',

      -- Adds a number of user-friendly snippets
      'rafamadriz/friendly-snippets',
    },
  },
  'weilbith/nvim-code-action-menu',
  { 'folke/which-key.nvim', opts = {} },
  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = false,
        theme = 'onedark',
        component_separators = '|',
        section_separators = '',
      },
    },
  },
  {
    -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      { 'williamboman/mason.nvim', config = true },
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
  },
  'tpope/vim-sleuth',
  {
    -- Add indentation guides even on blank lines
    'lukas-reineke/indent-blankline.nvim',
    -- Enable `lukas-reineke/indent-blankline.nvim`
    -- See `:help indent_blankline.txt`
    opts = {
      char = '┊',
      show_trailing_blankline_indent = false,
    },
  },
  { 'numToStr/Comment.nvim', opts = {} },
}
