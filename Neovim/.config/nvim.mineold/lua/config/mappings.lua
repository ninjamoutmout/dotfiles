-- [[ Basic Keymaps ]]

local keymap = vim.keymap.set
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
keymap({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
keymap('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Use ctrl+hjkl to move between wondows (only outside tmux, otherwise, it is handled by vim-tmux-navigator)
keymap("n", "<C-j>", "<C-w>j", { desc = "Down window" })
keymap("n", "<C-k>", "<C-w>k", { desc = "Up window" })
keymap("n", "<C-h>", "<C-w>h", { desc = "Left window" })
keymap("n", "<C-l>", "<C-w>l", { desc = "Right window" })

-- Resize with arrows
keymap("n", "<C-Up>", "<cmd>resize -2<cr>", { desc = "Resize window up" })
keymap("n", "<C-Down>", "<cmd>resize +2<cr>", { desc = "Resize window down" })
keymap("n", "<C-Left>", "<cmd>vertical resize -2<cr>", { desc = "Resize window left" })
keymap("n", "<C-Right>", "<cmd>vertical resize +2<cr>", { desc = "Resize window right" })

-- Fterm --
keymap("n", "<A-t>", '<CMD>lua require("FTerm").toggle()<CR>', { desc = "[T]oggle [T]erminal" })
keymap("t", "<A-t>", '<C-\\><C-n><CMD>lua require("FTerm").toggle()<CR>', { desc = "[T]oggle [T]erminal" })

-- System clipboard integration
keymap({ "n", "v" }, "gr", [["_]], { desc = "Motion without yanking" })
keymap({ "n", "v" }, "<leader>d", [["_d]], { desc = "[D]elete without yanking" })
keymap({ "n", "v" }, "gy", [["+y]], { desc = "Yank to sytem clipboard" })
keymap({ "n", "v" }, "gY", [["+y$]], { desc = "Yank to sytem clipboard" })

--Rebinding _ to \ 
-- keymap("i","\\", "_")
-- keymap("i","_", "\\")

--rebinding ç to ^
-- keymap("i","ç", "^")
-- keymap("i","^", "ç")

-- keymap({"n", "v"}, "<left>", "")
-- keymap({"n", "v"}, "<right>", "")
-- keymap({"n", "v"}, "<up>", "")
-- keymap({"n", "v"}, "<down>", "")
keymap("n", "<M-c>b", "<cmd>Run ./build.sh && bin/*<cr>", { desc = "[b]uild and run" })
