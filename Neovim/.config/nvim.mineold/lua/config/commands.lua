vim.api.nvim_create_user_command("Run", function(opts)
    local command = ""

    if opts.args ~= "" then
        command = opts.args
    else
        command = vim.fn.input("Enter a command: ")
    end

    vim.cmd("split term://" .. command)

    local terminal_win = vim.api.nvim_get_current_win() -- Get the terminal win
    vim.api.nvim_win_set_option(terminal_win, "number", false) -- Remove line numbers
    vim.api.nvim_win_set_option(terminal_win, "relativenumber", false) -- Remove line numbers
    vim.api.nvim_win_set_option(terminal_win, "winbar", "") -- Remove dropbar

    vim.cmd("startinsert")

end, { nargs = "?" })

