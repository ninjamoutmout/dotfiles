#!/bin/sh

echo "events:enter"
echo "name:Log out"
echo "name:Suspend"
echo "name:Hibernate"
echo "comment:Suspend and save memory to disk, shutting RAM off completely"
echo "name:Reboot"
echo "comment:Power off and boot back up"
echo "name:Power off"
echo "end"

IFS=$'\n'
read event
read input

echo "action:wait_and_close"

if [ "$input" -eq 0 ]; then
    sway exit
elif [ "$input" -eq 1 ]; then
    systemctl suspend
elif [ "$input" -eq 2 ]; then
    systemctl hibernate
elif [ "$input" -eq 3 ]; then
    systemctl reboot
elif [ "$input" -eq 4 ]; then
    systemctl poweroff
fi
